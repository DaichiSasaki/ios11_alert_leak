#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) UIAlertController* alert;
@end

@implementation ViewController

- (void)dealloc
{
	[self removeObserver:self forKeyPath:@"alert.accessibilityViewControllerForSizing" context:nil];
	NSLog(@"ViewController dealloc");
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[self addObserver:self forKeyPath:@"alert.accessibilityViewControllerForSizing" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld  context:nil];
}

- (IBAction)test:(id)sender {
	NSLog(@"start new alert");
	self.alert = [UIAlertController alertControllerWithTitle:@"test" message:@"test" preferredStyle:UIAlertControllerStyleAlert];
	[self.alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
		NSLog(@"done");
	}]];
	[self presentViewController:self.alert animated:YES completion:nil];
}

- (IBAction)close:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
	NSLog(@"%@ %@ %@", keyPath, object, change);
}

@end
