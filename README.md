# README #

iOS11で発生するUIAlertControllerの検証ソース

### 現象 ###

* iOS 11 で下記現象を確認した
- UIAlertController が呼び出し元の View Controller に強参照 accessibilityViewControllerForSizing (非公開プロパティ)を持ちつづけ、dismissViewController で消滅するときに参照を開放しない。
- 公開プロパティの presentingViewControllerの参照は解放される
- 呼び出し元の View Controller が呼び出される UIAlertController に強参照を持つ場合、相互（循環）参照になり、呼び出し元が画面から消滅しても、双方が開放されない結果になる。
- iOS 10 では、accessibilityViewControllerForSizing プロパティは存在せず、dismissViewControllerにより、呼び出し元の viewController への参照(presentingViewControllerのみ)は消滅し、循環参照は残らず、解放される。UIAlertController の後方非互換性（開放漏れ）と考えられる。

### テスト方法 ###

* step 1. ~ step 3.まで順に実行。step 4.の内容を確認する